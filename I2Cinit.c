/*! \file  I2Cinit.c
 *
 *  \brief Initializes the I2C peripheral
 *
 * The I2C peripheral 1 is connected to RB6 (SCL) and RB5 (SDA)
 * on the dsPIC33EVxxy02.
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:30 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"



/*! I2Cinit() - Initializes the I2C peripheral.   */
/*! This function sets the I2C baud rate, sets up master mode with
 *  no slew rate control, clears the transmit and receive
 *  registers and then enables the I2C peripheral.  The baud rate
 *  is calculated in i2c.h.
 *
 *  \param none
 *  \return none
 */
void I2Cinit(void)
{
  /* Neither RB5 nor RB6 have analog capability  */
  _TRISB5 = 0;
  _TRISB6 = 0;

    /* Now we will initialize the I2C peripheral for Master Mode,
     *  No Slew Rate Control, and leave the peripheral switched off.
     *
     * bit name   val meaning
     *  15 I2CEN   0  Enable (disable for now)
     *  14 unused  0
     *  13 I2CSIDL 0  Stop in idle mode (no)
     *  12 SCLREL  0  Release control bit (no - slave only)
     *  11 IPMIEN  0  IPMI enable (no)
     *  10 A10M    0  10-bit slave address (no - slave only)
     *   9 DISSLW  1  Disable slew rate control (yes)
     *   8 SMEN    0  SMBbus input levels (no)
     *   7 GCEN    0  General call enable (no - slave only)
     *   6 STREN   0  Clock stretch bit (no - slave only)
     *   5 ACKDT   0  Acknowledge data bit (no)
     *   4 ACKEN   0  Acknowledge sequence enable (no)
     *   3 RCEN    0  Receive enable (no)
     *   2 PEN     0  Stop condition enable (no)
     *   1 RSEN    0  Repeated start condition enable (no)
     *   0 SEN     0  Start condition enable (no)
     */
    /* I2C2CON1 = 0x0200; */

    /* /\* Set the I2C BRG Baud Rate.  The value for BRGVAL is */
    /*  * calculated in i2c.h *\/ */
    /* I2C2BRG = BRGVAL; */

    /* /\* Clear the receive and transmit buffers *\/ */
    /* I2C2RCV = 0x0000; */
    /* I2C2TRN = 0x0000; */

    /* /\* Now we can enable the peripheral *\/ */
    /* I2C2CON1bits.I2CEN = 1; */

  I2C2BRG = 0x188; /* 100 kHz DS70005319B p625 */
  I2C2CONLbits.I2CSIDL = 1;  /* Continue in idle mode */
  I2C2CONLbits.STRICT  = 0;  /* Strict mode not enforced */
  I2C2CONLbits.A10M    = 0;  /* 7-bit address */
  I2C2CONLbits.DISSLW  = 1;  /* Slew rate control disabled */
  I2C2CONLbits.SMEN    = 0;  /* Disable SMBus-specific inputs */
  I2C2CONLbits.ACKDT   = 0;  /* Send ACK sequence for acknowledge */

  I2C2CONHbits.SDAHT   = 1;  /*300ns hold time after falling edge of SCL */
  I2C2CONLbits.I2CEN   = 1;  /* Enable I2C #2 */



}
