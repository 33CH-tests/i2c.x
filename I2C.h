/*! \file I2C.h
 *
 *  \brief  Function prototypes and manifest constants for I2C library
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */
#ifndef I2C__H
#define I2C__H

#include <xc.h>

/*! Maximum length for get/put string */
#define PAGESIZE	32

/* Low Level Functions */

/*! Waits for bus to become Idle.   */
void I2Cidle(void);
/*! Generates an I2C Start Condition.   */
unsigned int I2Cstart(void);
/*! Writes a byte out to the bus   */
void I2Cwrite(unsigned char);
/*! Generates a bus stop condition.  */
unsigned int I2Cstop(void);
/*! Generates a restart condition and optionally returns status.   */
unsigned int I2Crestart(void);
/*! Initializes the I2C peripheral.   */
void I2Cinit(void);
/*! Read a single byte from Bus.  */
unsigned int I2Cget(void);
/*! Generates an Acknowledge.   */
//void I2CACK(void);
/*! Return the Acknowledge status on the bus   */
unsigned int I2CACKstatus( void );
/*! Generates a NO Acknowledge on the Bus   */
void I2CnotACK(void);

/* Calculate the value for the I2C baud rate generator */

/*! Instruction cycle time */
#ifndef FCY
#define FCY 70000000
#endif
/*! I2C baud rate */
#define FSCK 400000
/*! Value for baud rate generator */
//#define BRGVAL ( (FCY/FSCK) - (FCY/1111111) ) - 1
// 0x360 appears to be very close to 40 kHz
//#define BRGVAL (0x360)
// 0x57 should be close to 400 kHz, standard I2C rate
// but it measures closer to 29 kHz and getting quite rounded
#define BRGVAL (0x45)

#define AT24C16_ADDRESS 0xe9

#endif
