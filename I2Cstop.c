/*! \file  I2Cstop.c
 *
 *  \brief Generates a bus stop condition
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:37 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2Cstop() - Generates a bus stop condition.  */
/*! This function generates an I2C stop condition and returns status
 *  of the Stop.
 *
 *  \param none
 *  \return I2C2STATbits.P - stop condition detected bit
 */
unsigned int I2Cstop(void)
{
    I2Cidle();
    I2C2CON1bits.PEN = 1;             /* Generate stop condition      */
    while (I2C2CON1bits.PEN);         /* Wait for Stop                */
    return(I2C2STATbits.P);          /* return status                */
}

