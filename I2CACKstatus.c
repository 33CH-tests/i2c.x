/*! \file  I2CACKstatus.c
 *
 *  \brief Return the Acknowledge status on the bus
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:44 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2CACKstatus()- Return the Acknowledge status on the bus   */
/*! This function checks the state of the ACKSTAT bit in the I2C
 *  status register and returns TRUE if clear.
 *
 *  \param none
 *  \return Inverse of state of I2C2STATbits.ACKSTAT
 *
 */
unsigned int I2CACKstatus( void )
{
    return (!I2C2STATbits.ACKSTAT);
}
