/*! \file  I2Cidle.c
 *
 *  \brief Waits for bus to become Idle
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:33 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2Cidle() - Waits for bus to become Idle.   */
/*! Watches the transmit status bit and exits when the
 *  bit is cleared.
 *
 *  Technically this doesn't mean the bus is clear, but the part cannot
 *  transmit while a receive is in progress, so essentially, ensuring the
 *  transmit buffer is full is adequate.
 *
 *  \param none
 *  \return none
 */
void I2Cidle(void)
{
    while (I2C2STATbits.TRSTAT);     /* Wait for bus Idle        */
}
