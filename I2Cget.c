/*! \file  I2Cget.c
 *
 *  \brief Read a single byte from the I2C
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:41 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2Cget() - Read a single byte from Bus.  */
/*! Enables master receive then waits for the receive buffer to be full.
 *
 *  \param none
 *  \return Contents of I2C receive buffer
 *
 */
unsigned int I2Cget(void)
{
    I2Cidle();
    I2C2CON1bits.RCEN = 1;            /* Enable Master receive            */
    Nop();
    while(!I2C2STATbits.RBF);        /* Wait for receive bufer to be full*/
    return(I2C2RCV);                 /* Return data in buffer            */
}
