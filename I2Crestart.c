/*! \file  I2Crestart.c
 *
 *  \brief Generates a restart condition and returns the "Start heard last" bit
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:39 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2Crestart() - Generates a restart condition and returns the "Start heard last" bit.   */
/*! Function sets the restart bit and waits for the restart to happen. It
 *  then returns I2C2STATbits.S, the "Start heard last" bit.
 *
 *  \param none
 *  \return I2C2STATbits.S - start condition detected
 */
unsigned int I2Crestart(void)
{
    I2Cidle();
    //1 IFS0bits.MI2CIF = 0;
    I2C2CON1bits.RSEN = 1;            /* Generate restart             */
    while (I2C2CON1bits.RSEN);        /* Wait for restart             */
    //while ( !IFS0bits.MI2CIF);
    return(I2C2STATbits.S);          /* return status                */
}
